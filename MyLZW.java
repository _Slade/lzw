import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * This is a considerable modification to and extension of the LZW compression
 * code in Algorithms 4e by Robert Sedgewick and Kevin Wayne. It is far more
 * efficient and adds multiple new modes of compression (having to do with how
 * the dictionary is reset) which generally achieve better results than the
 * baseline algorithm.
 */
public class MyLZW
{
    private static final int          // Integer constants
        CHARS      = 1 << Byte.SIZE,  // Number of input chars
        INIT_WIDTH = Byte.SIZE + 1,   // Initial width of output code
        MAX_WIDTH  = 2 * Byte.SIZE,   // Maximum width of output code
        CODE_CNT   = 1 << INIT_WIDTH; // Number of codewords = 2^width

    private static boolean DEBUG;
    private static boolean QUIET;

    public static void compress(Flag mode)
    {
        if (DEBUG)
            System.err.println("Compressing with mode " + mode);
        if (!QUIET)
        {
            int modeMarker;
            switch (mode)
            {
                case NOTHING: modeMarker = 'N'; break;
                case RESET:   modeMarker = 'R'; break;
                case MONITOR: modeMarker = 'M'; break;
                default: throw new RuntimeException("Unexpected mode");
            }
            BinaryStdOut.write(modeMarker, Byte.SIZE);
        }

        final char[] input = BinaryStdIn.readString().toCharArray();
        int offset         = 0;
        // Size of our compressed output, in bits, starting at 8 due to modeMarker
        long cmpSize = 0;
        long expSize = 0;
        /* Instead of duplicating the initializations needed to reset, we can
        put all the initialization stuff into an infinite loop and put a `break
        RESET;` after our main loop. If we ever need to reset, we can just do
        `continue RESET;`. */
        int iterations = 0;
        RESET:
        for (;;)
        {
            // This is our map of strings -> codes
            TST<Integer> st = new TST<Integer>();
            initST(st);
            // We need mutability for these two
            int width          = INIT_WIDTH;
            int codeCount      = CODE_CNT;
            boolean monitoring = false;
            double oldRatio    = 1.0; // We calculate this once the codebook is full
            // Skip 256 (0x100) and use it as the EOF marker
            MAIN:
            for (int code = CHARS + 1; offset < input.length; )
            {
                ++iterations;
                // Longest prefix, which we emit a single code for
                final String chunk = st.longestPrefixOf(input, offset);
                final int chunkLen = chunk.length();
                // With every iteration, we print out our code
                if (!QUIET)
                    BinaryStdOut.write(st.get(chunk), width);

                cmpSize += width;
                expSize = (offset + chunkLen) * Byte.SIZE;
                double ratio = (double)expSize / (double)cmpSize;

                assert cmpSize >= 0 : "cmpSize = " + cmpSize;
                assert expSize >= 0 : "expSize = " + expSize;

                if (offset + chunkLen >= input.length)
                    break MAIN; // Reached EOF

                assert code  <= codeCount : "code <= codeCount: code = " + code;
                assert width <= MAX_WIDTH : "width <= MAX_WIDTH : width = " + width;

                if (code >= codeCount && width < MAX_WIDTH)
                {
                    ++width;
                    codeCount = 1 << width;
                }

                if (code < codeCount)
                {
                    st.put(new String(input, offset, chunkLen + 1), code);
                    ++code;
                }
                else if (code == codeCount && width == MAX_WIDTH)
                {
                    switch (mode)
                    {
                        case MONITOR:
                            if (!monitoring)
                            {
                                oldRatio   = ratio;
                                monitoring = true;
                                break;
                            }
                            else if (oldRatio / ratio <= 1.1)
                                break;
                            /* else fallthrough */
                        case RESET:
                            if (DEBUG)
                                System.err.printf("(compress) Resetting: i = %d, c = %d, e = %d%n", iterations, cmpSize / 8, expSize / 8);
                            offset += chunkLen;
                            continue RESET;
                        default: break;
                    }
                }
                else throw new RuntimeException("This shouldn't have happened");
                offset += chunkLen;
            } // End MAIN loop
            if (!QUIET)
                BinaryStdOut.write(CHARS, width);
            return;
        } // End RESET loop
    }

    public static void expand()
    {
        // This is our map of codes -> expanded strings
        List<String> st = new ArrayList<String>(CODE_CNT);
        initST(st);

        final Flag mode;
        {   // Just for local scope
            int modeByte = BinaryStdIn.readInt(Byte.SIZE);
            switch (modeByte)
            {
                case 'N': mode = Flag.NOTHING; break;
                case 'R': mode = Flag.RESET;   break;
                case 'M': mode = Flag.MONITOR; break;
                default:
                    throw new RuntimeException(
                        "Possibly corrupt file: could not recognize mode"
                    );
            }
        }

        long cmpSize   = 0;
        long expSize   = 0;
        double ratio   = 1.0;
        int iterations = 0;

        RESET:
        for (;;)
        {
            int width          = INIT_WIDTH;
            int codeCount      = CODE_CNT;
            boolean monitoring = false;
            double oldRatio    = 1.0;
            int codeword       = BinaryStdIn.readInt(width);
            if (codeword == CHARS) return;
            String val = st.get(codeword);
            MAIN:
            for (int code = CHARS + 1; true; )
            {
                ++iterations;
                cmpSize += width;
                expSize += val.length() * Byte.SIZE;
                ratio = (double)expSize / (double)cmpSize;

                if (code >= codeCount && width < MAX_WIDTH)
                {
                    ++width;
                    codeCount = 1 << width;
                }

                if (code < codeCount)
                {
                    ++code;
                }
                else if (code == codeCount && width == MAX_WIDTH)
                {
                    switch (mode)
                    {
                        case MONITOR:
                            if (!monitoring)
                            {
                                oldRatio   = ratio;
                                monitoring = true;
                                break;
                            }
                            else if (oldRatio / ratio <= 1.1)
                                break;
                        /* fallthrough */
                        case RESET:
                            if (DEBUG)
                                System.err.printf("(expand)   Resetting: i = %d, c = %d, e = %d%n", iterations, cmpSize / 8, expSize / 8);
                            st = new ArrayList<String>(CODE_CNT);
                            initST(st);
                            if (!QUIET)
                                BinaryStdOut.write(val);
                            continue RESET;
                        default: break;
                    }
                }

                if (!QUIET)
                    BinaryStdOut.write(val);

                codeword = BinaryStdIn.readInt(width);
                if (codeword == CHARS) { break; }

                String lookahead;
                if (codeword < st.size())
                    lookahead = st.get(codeword);
                else // if (st.size() - 1 == codeword)
                    lookahead = val + val.charAt(0); // Special case hack
                st.add(val + lookahead.charAt(0));
                val = lookahead;

                assert code  != 0 : "code overflowed";
                assert code  <= codeCount;
                assert width <= MAX_WIDTH;
            } // End MAIN loop
            break RESET;
        } // End RESET loop
    }

    private static String cpToString(int codepoint)
    {
        return new String(Character.toChars(codepoint));
    }

    private static void initST(TST<Integer> st)
    {
        for (int i = 0; i < CHARS; i++) // Add bytes 0 .. 255 to the table
            st.put(cpToString(i), i);
    }

    private static void initST(List<String> st)
    {
        for (int i = 0; i < CHARS; i++)
            st.add(cpToString(i));
        st.add("EOF"); // 256 lookahead for EOF
    }

    private static enum Flag
    {
        COMPRESS,
        EXPAND,
        NOTHING(true),
        RESET  (true),
        MONITOR(true),
        QUIET, // Suppress stdout
        DEBUG; // Output debugging messages on stderr
        private final boolean isMode;
        Flag()          { isMode = false; }
        Flag(boolean p) { isMode = p;     }
    }

    private static Flag strToFlag(String s)
    {
        switch (s)
        {
            case "-": case "--compress":           return Flag.COMPRESS;
            case "+": case "--expand":             return Flag.EXPAND;
            case "n": case "-n": case "--nothing": return Flag.NOTHING;
            case "r": case "-r": case "--reset":   return Flag.RESET;
            case "m": case "-m": case "--monitor": return Flag.MONITOR;
            case "-q": case "--quiet":             return Flag.QUIET;
            case "-d": case "--debug":             return Flag.DEBUG;
            default: throw new IllegalArgumentException("Unknown parameter: " + s);
        }
    }

    public static void main(String[] args)
    {
        final Set<Flag> flags = new TreeSet<>();
        for (String s : args) { flags.add(strToFlag(s)); }

        final boolean compress;
        if (flags.contains(Flag.COMPRESS) && flags.contains(Flag.EXPAND))
        {
            throw new IllegalArgumentException(
                "Cannot expand and compress at the same time"
            );
        }
        else if (!flags.contains(Flag.COMPRESS) && !flags.contains(Flag.EXPAND))
        {
            System.err.println(
                "Warning: - or + not specified, assuming -"
                + " (press CTRL-D to close input)"
            );
            compress = true;
        }
        else { compress = flags.contains(Flag.COMPRESS); }

        Flag mode      = Flag.NOTHING;
        int modesFound = 0;
        for (Flag f : flags)
        {
            if (f.isMode)
            {
                ++modesFound;
                mode = f;
            }
        }
        if (modesFound > 1)
            throw new IllegalArgumentException("Too many modes specified");
        if (modesFound != 0 && flags.contains(Flag.EXPAND))
            System.err.println(
                "Warning: MyLZW will automatically detect the necessary mode"
                + " for expansion; the flag you gave will be ignored."
            );

        DEBUG = flags.contains(Flag.DEBUG);
        QUIET = flags.contains(Flag.QUIET);

        // Closing during program execution interferes with -Xprof output
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() { BinaryStdOut.close(); }
        });

        if (compress)
            compress(mode);
        else
            expand();
    }
}

/*  MyLZW.java - Perform LZW compression and decompression with various modes. 
 *  Copyright (C) 2002-2015 Robert Sedgewick and Kevin Wayne, with extensive
 *  modifications Copyright (C) 2015 Christopher Constantino.
 *
 *  This file is a modification of LZW.java, a part of algs4.jar, which
 *  accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// vim:et:ts=4:sts=4:sw=4:ft=java
