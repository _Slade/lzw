JC = javac
JR = java
JCFLAGS = -Xlint
DEPS = $(addsuffix .java, BinaryStdIn BinaryStdOut Queue StdIn StdOut TST)

default : MyLZW.class

MyLZW.class : MyLZW.java $(DEPS)
	$(JC) $(JCFLAGS) $<

clean :
	$(RM) ./*.class
